json.array!(@checkouts) do |checkout|
  json.extract! checkout, :id, :customer_id, :cart_id, :payment, :date_time
  json.url checkout_url(checkout, format: :json)
end
