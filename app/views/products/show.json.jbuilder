json.extract! @product, :id, :description, :colour, :range, :flower_type, :image, :stock_qty, :price, :created_at, :updated_at
