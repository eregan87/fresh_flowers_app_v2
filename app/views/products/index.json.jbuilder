json.array!(@products) do |product|
  json.extract! product, :id, :description, :colour, :range, :flower_type, :image, :stock_qty, :price
  json.url product_url(product, format: :json)
end
