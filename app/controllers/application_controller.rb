class ApplicationController < ActionController::Base
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  protected
  
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :email, :phone_number, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:email, :first_name, :last_name, :phone_number, :password, :password_confirmation, :current_password) }
  end


  def setUpCategories
    @range = Category.where(:collection => 'Range')
    @colour = Category.where(:collection => 'Colour')
  end  

  def current_cart
   Order.find(session[:order_id])
   rescue ActiveRecord::RecordNotFound
   cart = Order.create
   session[:order_id] = :id
   cart
  end
  
  def after_sign_out_path_for(admin)
     root_path
  end
  
  def calculateTotal
    if session[:cartitem] != nil
    
    @total = 0
    #create an array to hold the products to render in the cart view
    @cartitems=[]
    
    #iterate through cartitem property array located in the session (these are product ids)
    session[:cartitem].each do |product_id|
      #myProduct is the variable that hold the array that is returns by the query on the database 
      myProduct = Product.where(:id => product_id)
      #push the first and only item in myProduct to the cart items array
      if myProduct.length == 1
        @cartitems.push(myProduct[0])
      end
    end  
    
    @cartitems.each do |product|
      
      if product.price
        @total = @total + product.price
      end
    end
    else
      @total = 0
    end
  end
  
  def numberOfCartItems
    @length = 0   #Set the number of items in the cart to 0 if noone is logged in
    
      if session[:cartitem] != nil  #If the array isn't null
        @length = session[:cartitem].length
      else
        @length = 0
      end
  end
    
    

end


