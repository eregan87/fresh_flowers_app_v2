class StaticPagesController < ApplicationController
  before_filter :numberOfCartItems
  before_filter :calculateTotal
  def home
  end

  def about
  end

  def contact
  end
  
  def thankyou
  end
  
end
