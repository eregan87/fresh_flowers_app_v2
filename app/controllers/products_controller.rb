class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_filter :numberOfCartItems
  before_filter :calculateTotal
  before_filter :setUpCategories
  
  
  #Only allows admins to access these controller actions
  before_filter :authenticate_admin!, :only => [:new, :edit, :create, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
  #filter by colour, range, price
  if params[:category]
   @products = Product.joins(:product_categories).where('product_categories.category_id' => params[:category]).paginate(:page => params[:page], :per_page => 6)
  elsif params[:a]
   @products = Product.where('products.price < 30').paginate(:page => params[:page], :per_page => 6)
  elsif params[:b]
   @products = Product.where('products.price < 50 AND products.price >= 30').paginate(:page => params[:page], :per_page => 6) 
  elsif params[:c]
   @products = Product.where('products.price < 70 AND products.price >= 50').paginate(:page => params[:page], :per_page => 6)
  elsif params[:d]
   @products = Product.where('products.price > 70').paginate(:page => params[:page], :per_page => 6) 
  else  
    @products = Product.all.paginate(:page => params[:page], :per_page => 6)
  end

  end

  # GET /products/1
  # GET /products/1.json
  def show
  end
  
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
    #retrieve the preiously assigned product categories
    setUpProductCategories
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    
    isSaved = @product.save
    
    #populate product_category table upon product creation
    params['category'].each do | categoryId | 
      newProductCategories = ProductCategory.new(product_id: @product.id, category_id: categoryId)
      @isSaved2 = newProductCategories.save
    end

    respond_to do |format|
      if isSaved and @isSaved2
        format.html { redirect_to @product }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    
    ProductCategory.destroy_all(:product_id => params[:id])
    #reassign categories
    params['category'].each do | categoryId | 
      newProductCategories = ProductCategory.new(product_id: @product.id, category_id: categoryId)
      @isSaved2 = newProductCategories.save
    end
    
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url}
      format.json { head :no_content }
    end
  end

  def setUpProductCategories
  #retrieve previously assigned product categories
  @pcArray= ProductCategory.where(:product_id => params[:id])
  
  @categoryIds = []
  
  @pcArray.each do |pc|
    
    @categoryIds.push(pc.category_id)
    
    end
    
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:description, :image, :stock_qty, :price, :title)
    end
end
