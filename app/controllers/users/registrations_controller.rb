class Users::RegistrationsController < Devise::RegistrationsController
    before_filter :numberOfCartItems
    before_filter :calculateTotal
# before_filter :configure_sign_up_params, only: [:create]
# before_filter :configure_account_update_params, only: [:update]

#  
#  before_filter :configure_permitted_parameters

#  protected

  # If you have extra params to permit, append them to the sanitizer.
  #Allows other fields to be used at sign_up
#  def configure_permitted_parameters
#    devise_parameter_sanitizer.for(:sign_up) do |u|
#      u.permit(:first_name, :last_name, :phone_number,
#        :email, :password, :password_confirmation)
#        .require(:first_name, :last_name,
#        :email, :password, :password_confirmation)
#    end
    
  # If you have extra params to permit, append them to the sanitizer.
#    devise_parameter_sanitizer.for(:account_update) do |u|
#      u.permit(:first_name, :last_name, phone_number,
#        :email, :password, :password_confirmation, :current_password)
#    end
#  end
end

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
