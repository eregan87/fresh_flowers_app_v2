class CartController < ApplicationController
    before_filter :numberOfCartItems
    before_filter :calculateTotal
    
  def index
    
    if !session[:cartitem]
       session[:cartitem] = []
    end
    #create an array to hold the products to render in the cart view
    @cartitems=[]
    
    #iterate through cartitem property array located in the session (these are product ids)
    session[:cartitem].each do |product_id|
      #myProduct is the variable that hold the array that is returns by the query on the database 
      myProduct = Product.where(:id => product_id)
      #push the first and only item in myProduct to the cart items array
      @cartitems.push(myProduct[0])
    end 
    
    calculateTotal
    
  end
  
  
  def create
    
    if !session[:cartitem]
       session[:cartitem] = []
    end
    
    session[:cartitem].push(params[:id])
    
    respond_to do |format|
      format.html { redirect_to :cart }
    end
  
  end
  
  def removeCartItem
    
    @cart = session[:cartitem]
    
    @cart.delete_at(@cart.index(params[:id]))
    
    respond_to do |format|
      format.html { redirect_to :back }
    end
    
  end
  
end
