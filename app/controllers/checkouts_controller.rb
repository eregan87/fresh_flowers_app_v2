class CheckoutsController < ApplicationController
  before_action :set_checkout, only: [:show, :edit, :update, :destroy]
  #Only allows admins to access these controller actions
  before_filter :authenticate_admin!, :only => [:edit, :update, :destroy, :show]
  before_filter :numberOfCartItems
  before_filter :calculateTotal

  # GET /checkouts
  # GET /checkouts.json
  def index

#if an admin is logged in
    if current_admin
     @checkouts = Checkout.all
    end
#if a user is logged in
    if current_user
#Find checkouts for which the customer id is equal to the id of 
#the current_user (logged into session)
     @checkouts = Checkout.where(:customer_id => current_user.id)
    end
    
  end

  # GET /checkouts/1
  # GET /checkouts/1.json
  def show
  end

  # GET /checkouts/new
  #Automatically fill in checkout form using the values below
  #Only some of these attributes will be shown to the user so they can be edited
  def new
    @checkout = Checkout.new
    #(:customer_id => current_user.id, :date_time => "Time.now", :payment => "false", 
    #:deliver_to_address => "Enter address", :credit_card_number => "Enter credit card number",
    #:total_price => calculateTotal)
  end

  # GET /checkouts/1/edit
  def edit
  end

  # POST /checkouts
  # POST /checkouts.json
  def create
    @checkout = Checkout.new(checkout_params)

    respond_to do |format|
      if @checkout.save
        #redirect_to thankyou page after purchase
        format.html {redirect_to thankyou_path, notice: 'Your checkout was successful.' }
        format.json { render :show, status: :created, location: @checkout }
          @checkout.update(:payment => "true")  #Set payment attribute to true if the checkout goes through
          emptycart #Remove all products from cart
      else
      redirect_to notice: 'Checkout failed. Please try again.'
        format.html { render :new }
      format.json { render json: @checkout.errors, status: :unprocessable_entity }
      end
    end
  end
  
  #Method to remove all products from cart on checkout
  def emptycart

  @cart = session[:cartitem]  #Set @cart as the aray of cart items in the session
    
  @cart.clear #Clear the array

  #Other attempts to empty the cart!!
   #Array of cart items
   #len = (session[:cartitem].length-1)  #length of array of cart items
   
   # x = 0 #counter
    
    #While there are items in the cart
    #until session[:cartitem].empty? do
    
     # if session[:cartitem] != nil
    #Remove the item at index x 
    #session[:cartitem].delete_at(x)
    #x += 1  #Increment x
    #  end
      
    #end
    
  #  for i in 0..len do
  #    session[:cartitem].delete_at(i)
  #  end
  end

  # PATCH/PUT /checkouts/1
  # PATCH/PUT /checkouts/1.json
  def update
    respond_to do |format|
      if @checkout.update(checkout_params)
        format.html { redirect_to @checkout, notice: 'Checkout was successfully updated.' }
        format.json { render :show, status: :ok, location: @checkout }
      else
        format.html { render :edit }
        format.json { render json: @checkout.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /checkouts/1
  # DELETE /checkouts/1.json
  def destroy
    @checkout.destroy
    respond_to do |format|
      format.html { redirect_to checkouts_url, notice: 'Checkout was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_checkout
      @checkout = Checkout.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def checkout_params
      params.require(:checkout).permit(:customer_id, :total_price, :payment, :date_time, :deliver_to_address, :credit_card_number)
    end
end
