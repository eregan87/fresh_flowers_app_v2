class Checkout < ActiveRecord::Base
  belongs_to :user
  
  #I can't do this becasue I use a render in the action of the button click
  #and you can only have one render/redirect per action
  #Validate delivery address and credit card number for checkouts
  #validates :deliver_to_address, presence: true, length:{minimum:5, maximum:255}
  
  #validates :credit_card_number, presence: true, length:{minimum:16, maximum:19}
end
