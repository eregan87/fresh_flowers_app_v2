class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :rememberable
  #:registerable - We don't make admins registerable to improve security
  #attr_accessible :email, :password, :password_confirmation
end
