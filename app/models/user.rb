class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  #Relationship user-address is 1-*
  has_many :addresses
  #Relationship user-order is 1-*
  has_many :orders
  has_many :checkouts
  
  #Validation of input when user is created
  #These fields cannot be blank and some have minimum/maximum lengths
  validates :first_name, presence: true, length:{maximum:50}
  
  validates :last_name, presence: true, length:{maximum:50}
  
  validates :email, presence: true, length:{minimum:5, maximum:255},
             uniqueness: true
             
  #Don't validate password because devise does this anyway and 
  #if we validate here we can't leave black on the update form
end