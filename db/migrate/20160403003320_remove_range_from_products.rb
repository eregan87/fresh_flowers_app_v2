class RemoveRangeFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :range, :string
  end
end
