class RemoveColumns < ActiveRecord::Migration
  
  #Method to remove the email column from the user model (so it can be readded using devise)
  def self.up
    remove_column :users, :email
  end

end
