class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :user_name
      t.string :password
      t.string :phone_number
      t.string :email
      t.integer :credit_card_number
      t.boolean :admin_access

      t.timestamps null: false
    end
  end
end
