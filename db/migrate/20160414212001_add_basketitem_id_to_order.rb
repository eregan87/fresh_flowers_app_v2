class AddBasketitemIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :basketitem_id, :integer
  end
end
