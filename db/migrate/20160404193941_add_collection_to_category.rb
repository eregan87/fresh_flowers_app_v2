class AddCollectionToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :collection, :string
  end
end
