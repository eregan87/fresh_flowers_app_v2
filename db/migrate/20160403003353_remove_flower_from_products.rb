class RemoveFlowerFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :flower_type, :string
  end
end
