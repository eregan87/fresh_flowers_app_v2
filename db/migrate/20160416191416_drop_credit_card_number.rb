class DropCreditCardNumber < ActiveRecord::Migration
  def change
    remove_column :users, :credit_card_number
  end
end
