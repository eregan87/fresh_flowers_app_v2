class DefaultForAdmin < ActiveRecord::Migration
  
  def change
    change_column :users, :admin_access, :boolean, :default => false
  end
  
end
