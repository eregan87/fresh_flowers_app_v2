class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :description
      t.string :colour
      t.string :range
      t.string :flower_type
      t.string :image
      t.integer :stock_qty
      t.decimal :price

      t.timestamps null: false
    end
  end
end
