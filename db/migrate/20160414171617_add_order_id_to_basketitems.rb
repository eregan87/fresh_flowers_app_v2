class AddOrderIdToBasketitems < ActiveRecord::Migration
  def change
    add_column :basketitems, :order_id, :integer
  end
end
