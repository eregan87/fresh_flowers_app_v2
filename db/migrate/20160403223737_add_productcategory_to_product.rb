class AddProductcategoryToProduct < ActiveRecord::Migration
  def change
    add_reference :products, :productcategory, index: true, foreign_key: true
  end
end
