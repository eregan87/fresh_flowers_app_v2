class EditCheckoutCols < ActiveRecord::Migration
  def self.down
    add_column :checkouts, :deliver_to_address, :string
    add_column :checkouts, :credit_card_number, :string
  end
  
  def self.up
    remove_column :checkouts, :card_id
  end
end
