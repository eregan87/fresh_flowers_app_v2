class AddCards < ActiveRecord::Migration
  def change
    add_column :checkouts, :deliver_to_address, :string
    add_column :checkouts, :credit_card_number, :string
  end
end
