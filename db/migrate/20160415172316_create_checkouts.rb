class CreateCheckouts < ActiveRecord::Migration
  def change
    create_table :checkouts do |t|
      t.integer :customer_id
      t.integer :cart_id #:default => order.id...I'd like to default this but I don't
                          # know how!      
      t.boolean :payment, :default => false
      t.datetime :date_time, :default => Time.now
      t.timestamps null: false
    end
  end
end
