class AddCategoryToProductCategory < ActiveRecord::Migration
  def change
    add_reference :product_categories, :category, index: true, foreign_key: true
  end
end
