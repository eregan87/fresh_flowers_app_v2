class RemoveCartId < ActiveRecord::Migration
  def change
    remove_column :checkouts, :cart_id
  end
end
