class RemoveProductcategoryIdFromCategories < ActiveRecord::Migration
  def change
    remove_column :categories, :productcategory_id, :integer
  end
end
