class AddProductcategoryToCategory < ActiveRecord::Migration
  def change
    add_reference :categories, :productcategory, index: true, foreign_key: true
  end
end
