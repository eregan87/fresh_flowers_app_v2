class AddTotalPriceToCheckout < ActiveRecord::Migration
  def change
    add_column :checkouts, :total_price, :decimal
  end
end
