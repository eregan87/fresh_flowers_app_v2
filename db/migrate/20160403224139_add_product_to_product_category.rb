class AddProductToProductCategory < ActiveRecord::Migration
  def change
    add_reference :product_categories, :product, index: true, foreign_key: true
  end
end
